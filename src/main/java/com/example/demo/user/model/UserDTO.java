package com.example.demo.user.model;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO {

    private String username;

    private String email;
    private String password;
    private Long id;

    public static User mapper(UserDTO dto) {
        if (Objects.isNull(dto))
            return null;
        return User.builder().email(dto.getEmail()).username(dto.getUsername()).password(dto.getPassword()).build();

    }

}
