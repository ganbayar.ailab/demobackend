package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.model.UserDTO;
import com.example.demo.user.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.*;


@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Page<User> getUsers(int pageNumber, int pageSize)
    {
        Pageable pages = PageRequest.of(pageNumber, pageSize);
        return userRepository.findAll(pages);
    }
    public User getUser(Long id)
    {
        return userRepository.findById(id).orElseThrow();
    }

    public User saveUser(User user)
    {
        return this.userRepository.save(user);
    }

    public User updateUser(UserDTO dto, Long id) {
        User oldUser = this.getUser(id);
        User user = UserDTO.mapper(dto);
        if (user != null) {
            user.setId(id);
        }
        try {
            System.out.println(updatedFields(oldUser, user));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return saveUser(user);
    }
    private static Map<String, Object> updatedFields(Object oldObject, Object newObject) throws IllegalAccessException {
        Map<String, Object> changedProperties = new HashMap<>();
        for (Field field : oldObject.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object oldValue = field.get(oldObject);
            Object newValue = field.get(newObject);
            if (oldValue != null && newValue != null) {
                if (!Objects.equals(oldValue, newValue)) {
                    changedProperties.put(field.getName(), newValue);
                }
            }
        }
        return changedProperties;
    }
    public void deleteUser(Long id)
    {
        userRepository.deleteById(id);
    }

}
